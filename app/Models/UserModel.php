<?php


namespace App\Models;


class UsersModel extends BaseModel{
    protected $table="users";

    protected $fillable = [
        'email',
        'phone',
    ];

}
